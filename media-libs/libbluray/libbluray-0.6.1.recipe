SUMMARY="Portable Network Graphics library"
DESCRIPTION="
libbluray is the official PNG reference library. It supports almost all PNG \
features, is extensible, and has been extensively tested for over 17 years
"
HOMEPAGE="http://www.videolan.org/developers/libbluray.html"
COPYRIGHT="
	1998-2011 Glenn Randers-Pehrson
	1996-1997 Andreas Dilger
	1995-1996 Guy Eric Schalnat, Group 42, Inc.
	"
LICENSE="LibPNG"
SRC_URI="http://ftp.videolan.org/pub/videolan/libbluray/$portVersion/libbluray-$portVersion.tar.bz2"
CHECKSUM_SHA256="bf51c4fb65f4d8b02e9b2708017ecd829c8d0dfa9239bca011e67a2a6e368b3f"
REVISION="1"
ARCHITECTURES="x86 x86_64"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
    # x86_gcc2 is fine as primary target architecture as long as we're building
    # for a different secondary architecture.
    ARCHITECTURES="$ARCHITECTURES x86_gcc2"
fi
SECONDARY_ARCHITECTURES="x86"

PROVIDES="
	libbluray$secondaryArchSuffix = $portVersion compat >= 0.6
	lib:libbluray$secondaryArchSuffix = 1.6.1 compat >= 1
	cmd:bd_info$secondaryArchSuffix
	"
REQUIRES="
	haiku$secondaryArchSuffix
	lib:libfreetype$secondaryArchSuffix
	lib:libxml2$secondaryArchSuffix
	# required by freetype
	lib:libbz2$secondaryArchSuffix
	lib:libpng$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel
	devel:libfreetype$secondaryArchSuffix
	devel:libxml2$secondaryArchSuffix
	# required by freetype
	devel:libbz2$secondaryArchSuffix
	devel:libpng$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	cmd:aclocal
	cmd:autoconf
	cmd:automake
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:libtoolize
	cmd:make
	cmd:pkg_config$secondaryArchSuffix
	"

BUILD()
{
	autoreconf -fi
	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install

	prepareInstalledDevelLibs libbluray
	fixPkgconfig

	# devel package
	packageEntries devel \
		$developDir
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	libbluray${secondaryArchSuffix}_devel = $portVersion compat >= 0.6
	devel:libbluray$secondaryArchSuffix = 1.6.1 compat >= 1
	"
REQUIRES_devel="
	libbluray$secondaryArchSuffix == $portVersion base
	"
