SUMMARY="integrated development environment for yab."
DESCRIPTION="
Yab allows fast prototyping with simple and clean code. yab contains a large \
number of BeAPI specific commands for GUI creation and much, much more.

yab wouldn't be complete without the yab-IDE, a powerful development \
environment, which of course is programmed in yab itself.
"
HOMEPAGE="http://sourceforge.net/projects/yab-interpreter"
SRC_URI="git+https://github.com/HaikuArchives/Yab.git#4014a60fd7f9bb786d1cf52e11483dee2d2f4d00"
REVISION="1"

LICENSE="Artistic"
COPYRIGHT="1995-2006 Marc-Oliver Ihm (yabasic)
	2006-2009 Jan Bungeroth (yab improvements)
	2013 Jim Saxton (yab improvements)"

ARCHITECTURES="x86_gcc2 x86 ?x86_64"

PROVIDES="
	yab_ide$secondaryArchSuffix = $portVersion
	app:yab_IDE = $portVersion
		"

REQUIRES="
	haiku$secondaryArchSuffix
	lib:libncurses$secondaryArchSuffix
	yab$secondaryArchSuffix >= 1.7.02
	devel:libncurses$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	"

BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel
	devel:libncurses$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
"

BUILD_PREREQUIRES="
	cmd:bison
	cmd:flex
	cmd:gcc$secondaryArchSuffix
	cmd:make
	cmd:mkdepend
	cmd:perl
	makefile_engine
	"

BUILD()
{
	cd src
	make $jobArgs BUILDHOME=`finddir B_SYSTEM_DEVELOP_DIRECTORY`
	unzip -o App_YAB.zip
	copyattr App_YAB yab
}

INSTALL()
{
	mkdir -p $binDir
	mkdir -p tmp
	mkdir -p tmp/buildfactory

	cp -r src/* tmp/buildfactory
	cp -r yab-IDE/BuildFactory/* tmp/buildfactory/
	unzip -o tmp/buildfactory/parts/yabstuff.zip -d tmp/buildfactory/parts
	cp yab-IDE/src/yab-IDE.yab  tmp/buildfactory/

	pushd tmp/buildfactory
	BuildFactory.yab yab-IDE yab-IDE.yab application/x-vnd.yab-IDE
	popd

	mkdir -p $appsDir
	cp -r yab-IDE/ $appsDir/
	cp  tmp/buildfactory/yab-IDE $appsDir/yab-IDE/
	cp yab-IDE/src/yab-IDE.yab $appsDir/yab-IDE/src/

	addAppDeskbarSymlink $appsDir/yab-IDE/yab-IDE "Yab IDE"
}
