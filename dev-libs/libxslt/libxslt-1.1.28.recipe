SUMMARY="XSLT C library developed for the GNOME project" 
HOMEPAGE="http://www.xmlsoft.org/" 
LICENSE="MIT"
COPYRIGHT="2001-2002 Daniel Veillard.  All Rights Reserved."
SRC_URI="ftp://xmlsoft.org/libxml2/libxslt-1.1.28.tar.gz"
CHECKSUM_SHA256="5fc7151a57b89c03d7b825df5a0fae0a8d5f05674c0e7cf2937ecec4d54a028c"
REVISION="3"
ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PROVIDES="
	libxslt$secondaryArchSuffix = $portVersion
	lib:libxslt$secondaryArchSuffix = $portVersion compat >= 1
	lib:libexslt$secondaryArchSuffix = 0.8.17 compat >= 0
	cmd:xslt_config$secondaryArchSuffix = $portVersion
	cmd:xsltproc$secondaryArchSuffix = $portVersion
	"

REQUIRES="
	haiku${secondaryArchSuffix}
	lib:libxml2$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	"
BUILD_REQUIRES="
	devel:libxml2$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	haiku_devel
	haiku${secondaryArchSuffix}_devel
	cmd:autoconf
	cmd:automake
	cmd:libtoolize
	cmd:gcc$secondaryArchSuffix
	cmd:make
	cmd:sed
	"

BUILD()
{
	libtoolize --force --copy --install
	aclocal
	autoconf
	automake

	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install

	prepareInstalledDevelLibs libxslt
	prepareInstalledDevelLibs libexslt
	fixPkgconfig
	
	# devel package
	packageEntries devel $developDir
}

DESCRIPTION="
Libxslt is the XSLT C library developed for the GNOME project. XSLT itself is \
a an XML language to define transformation for XML. Libxslt is based on \
libxml2 the XML C library developed for the GNOME project. It also implements \
most of the EXSLT set of processor-portable extensions functions and some of \
Saxon's evaluate and expressions extensions.
People can either embed the library in their application or use xsltproc the \
command line processing tool. This library is free software and can be reused \
in commercial applications.
"

PROVIDES_devel="
	libxslt${secondaryArchSuffix}_devel = $portVersion
	devel:libxslt${secondaryArchSuffix} = $portVersion compat >= 1
	devel:libexslt${secondaryArchSuffix} = 0.8.17 compat >= 0
	"
REQUIRES_devel="
	libxslt${secondaryArchSuffix} == $portVersion base
	"
